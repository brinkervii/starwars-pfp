import base64
import io
import os

from flask import Flask, render_template, flash
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from werkzeug.utils import redirect
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

import starwars_pfp

app = Flask(__name__, template_folder="templates")
app.config.update(SECRET_KEY=b"\x00" + os.urandom(200) + b"\x00")

Bootstrap(app)


class RobloxUserForm(FlaskForm):
    username = StringField("Roblox Username", validators=[DataRequired()])
    submit = SubmitField("Submit")


@app.route("/", methods=("GET", "POST"))
def index():
    form = RobloxUserForm()

    if form.validate_on_submit():
        try:
            username = form.username.data
            headshot = starwars_pfp.get_headshot(starwars_pfp.get_user_id(username))

            fp = io.BytesIO()
            starwars_pfp.superimpose(
                starwars_pfp.TEMPLATE_BG,
                starwars_pfp.TEMPLATE_FG,
                headshot,
                output_fp=fp
            )

            fp.seek(0)
            data = fp.read()
            fp.close()

            image = base64.b64encode(data).decode("UTF-8")
            image = "data:image/png;base64," + image

            return render_template("index.html", form=form, image=image)

        except Exception as e:
            flash("Something went terribly wrong! Are you using the right username?")
            return redirect("/")

    return render_template("index.html", form=form)


if __name__ == '__main__':
    app.run(debug=("DEBUG" in os.environ and os.environ["DEBUG"] == "1"))
