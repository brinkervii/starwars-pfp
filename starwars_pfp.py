import io
import os

import requests
from PIL import Image

HERE = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(HERE, "pfp_template_bg.png"), "rb") as TEMPLATE_BG_FP:
    TEMPLATE_BG = TEMPLATE_BG_FP.read()

with open(os.path.join(HERE, "pfp_template_fg.png"), "rb") as TEMPLATE_FG_FP:
    TEMPLATE_FG = TEMPLATE_FG_FP.read()


def get_user_id(username):
    url = f"https://api.roblox.com/users/get-by-username?username={username}"
    response = requests.get(url)
    assert 200 <= response.status_code < 300

    return int(response.json()["Id"])


def get_headshot_url(user_id, size=(150, 150)):
    url = f"https://thumbnails.roblox.com/v1/users/avatar-headshot?size={size[0]}x{size[1]}&format=png&userIds={user_id}"
    response = requests.get(url)
    assert 200 <= response.status_code < 300

    return response.json()["data"][0]["imageUrl"]


def get_headshot(user_id):
    url = get_headshot_url(user_id)
    response = requests.get(url)
    assert 200 <= response.status_code < 300

    return response.content


def superimpose(template_bg, template_fg, headshot, output_fp=None):
    template_bg_fp = io.BytesIO(template_bg)
    template_fg_fp = io.BytesIO(template_fg)
    headshot_fp = io.BytesIO(headshot)

    template_bg_image = Image.open(fp=template_bg_fp)
    template_fg_image = Image.open(fp=template_fg_fp)
    headshot_image = Image.open(fp=headshot_fp).resize((450, 450), Image.LANCZOS)

    location = (
        template_bg_image.size[0] / 2 - headshot_image.size[0] / 2,
        (template_bg_image.size[1] / 2 - headshot_image.size[1] / 2) + 50
    )
    location = tuple(map(int, location))

    template_bg_image.paste(headshot_image, location, headshot_image)
    template_bg_image.paste(template_fg_image, (0, 0), template_fg_image)

    if output_fp is not None:
        template_bg_image.save(output_fp, "PNG")

    else:
        template_bg_image.save("pfp.png", "PNG")

    for fp in [template_bg_fp, template_fg_fp, headshot_fp]:
        fp.close()
